package com.example.fininfocomtask;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Context;
import android.graphics.Color;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

public class MainActivity extends AppCompatActivity {
    EditText editText;
    Button button;
    RadioGroup radioGroup;
    RadioButton radioButton;
    GridLayout gridLayout;
    int  redCounter , equalCount ,greenCounter,blueCounter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        editText = findViewById(R.id.editText);
        button = findViewById(R.id.submit);
        radioGroup = findViewById(R.id.radioGroup);
        gridLayout =findViewById(R.id.gridview);
        redCounter = 0;
        greenCounter = 0;
        blueCounter = 0;

        mainTask();
        radioGroupChecking();
    }

    private void radioGroupChecking() {
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                int id=group.getCheckedRadioButtonId();
                radioButton= findViewById(id);
            }
        });
    }

    private void mainTask() {
        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Removing views from grid layout and resetting counter to zero
                gridLayout.removeAllViews();
                redCounter = 0;
                greenCounter = 0;
                blueCounter = 0;
                //checking validations of EditText
                if (TextUtils.isEmpty(editText.getText().toString())){
                    Toast.makeText(MainActivity.this, "Please enter a number", Toast.LENGTH_SHORT).show();

                }
                else {
                    final int num = Integer.parseInt(editText.getText().toString());
                    if (num % 3 == 0){
                        for (int i = 0; i < num; i++) {
                            //Inflating Layouts like boxes
                            LayoutInflater layoutInflater = (LayoutInflater) getLayoutInflater().getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                            View vv = layoutInflater.inflate(R.layout.item, gridLayout, false);
                            final LinearLayout linearLayout =vv.findViewById(R.id.box);
                            linearLayout.setOnClickListener(new View.OnClickListener() {
                                @Override
                                public void onClick(View v) {
                                    //Checking if any radio button is selected or not
                                    if (radioGroup.getCheckedRadioButtonId()== -1)
                                    {
                                        Toast.makeText(MainActivity.this, "Please select a color", Toast.LENGTH_SHORT).show();
                                    }
                                    else
                                    {
                                        //checking if red is selected and if selected, performing the task
                                        if (radioButton.getText().toString().equals("Red")){
                                            redCounter++;
                                            equalCount = num / 3;
                                            if (redCounter > equalCount){
                                                Log.d("TAG", "equalcounter: "+equalCount);
                                                Toast.makeText(MainActivity.this, "Limit Exceeded", Toast.LENGTH_SHORT).show();
                                            } else {
                                                linearLayout.setBackgroundColor(Color.RED);
                                                gridLayout.getChildCount();
                                                Log.d("TAG", "gridcount: "+gridLayout.getChildCount());
                                                Log.d("TAG", "equalcounter: "+equalCount);
                                            }


                                            //checking if green is selected and if selected, performing the task
                                        } else if (radioButton.getText().toString().equals("Green")){
                                            greenCounter++;
                                            equalCount = num / 3;
                                            if (greenCounter > equalCount)
                                            {
                                                Toast.makeText(MainActivity.this, "Limit Exceeded", Toast.LENGTH_SHORT).show();
                                            }
                                            else
                                            {
                                                linearLayout.setBackgroundColor(Color.GREEN);
                                            }
                                        }
                                        //checking if blue is selected and if selected, performing the task
                                        else if (radioButton.getText().toString().equals("Blue")){
                                            blueCounter++;
                                            equalCount = num / 3;
                                            if (blueCounter > equalCount){
                                                Toast.makeText(MainActivity.this, "Limit Exceeded", Toast.LENGTH_SHORT).show();
                                            } else {
                                                linearLayout.setBackgroundColor(Color.BLUE);
                                            }
                                        }
                                    }
                                }
                            });
                            //Removing color on long press
                            linearLayout.setOnLongClickListener(new View.OnLongClickListener() {
                                @Override
                                public boolean onLongClick(View v) {
                                    linearLayout.setBackgroundColor(Color.BLACK);
                                    redCounter--;
                                    greenCounter--;
                                    blueCounter--;
                                    return true;
                                }
                            });
                            gridLayout.addView(vv);
                            gridLayout.setColumnCount(3);
                        }
                    }
                    else
                    {
                        Toast.makeText(MainActivity.this, "Entered Number Should Be Multiple Of 3", Toast.LENGTH_SHORT).show();
                    }
                }
            }

        });
    }
}